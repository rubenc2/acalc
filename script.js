document.addEventListener('DOMContentLoaded', function() {
    document.getElementById('calcType').addEventListener('change', function() {
        const calcType = this.value;
    
        // Reset input areas and results without resetting the dropdown
        clearCalculator(false); // Pass 'false' to avoid resetting the dropdown

        // Now, adjust visibility and prepare UI based on the selected calculation type

        const nmFields = document.getElementById('nmFields');
        const inputsContainer = document.getElementById('inputs');
        const switchFieldContainer = document.getElementById('switchAvailability').parentElement;

        nmFields.style.display = (calcType === 'NM' || calcType === 'NMWithSwitch') ? 'block' : 'none';
        inputsContainer.style.display = (calcType === 'Series' || calcType === 'Parallel') ? 'block' : 'none';
        switchFieldContainer.style.display = (calcType === 'NMWithSwitch') ? 'block' : 'none';

        if (calcType === 'Series' || calcType === 'Parallel') {
            addAvailabilityInput(); // For series/parallel, start with one input field
        }
    });

    document.getElementById('add').addEventListener('click', function() {
        addAvailabilityInput();
    });

    document.getElementById('calculate').addEventListener('click', function() {
        calculateAvailability();
    });

    document.getElementById('clear').addEventListener('click', function() {
    // Set the dropdown back to the default "Select Type" option
    document.getElementById('calcType').selectedIndex = 0;
    
    // Trigger the change event for the dropdown to run any associated event handlers
    // This ensures that any logic tied to changing the selection is executed, such as clearing inputs or resetting the UI
    var event = new Event('change');
    document.getElementById('calcType').dispatchEvent(event);
    });
});



function handleCalcTypeChange(calcType) {
    // First, clear the calculator to reset inputs and visibility

    const nmFields = document.getElementById('nmFields');
    const inputsContainer = document.getElementById('inputs');
    const switchFieldContainer = document.getElementById('switchAvailability').parentElement;

    // Hide switch availability and N:M fields by default
    switchFieldContainer.style.display = 'none';
    nmFields.style.display = 'none';

    if (calcType === 'Series' || calcType === 'Parallel') {
        // Only for Series and Parallel: show inputs container and add an initial input
        inputsContainer.style.display = 'block';
        addAvailabilityInput();
    } else if (calcType === 'NM') {
        // For N:M without switches: show N:M fields and keep switch field hidden
        nmFields.style.display = 'block';
    } else if (calcType === 'NMWithSwitch') {
        // For N:M with switches: show both N:M fields and the switch field
        nmFields.style.display = 'block';
        switchFieldContainer.style.display = 'block';
    }
    // No need for an 'else' case here since 'clearCalculator' already handles the reset
    const configVisual = document.getElementById('configVisual');

    // Ensure configVisual is shown or hidden based on calcType without being persistently hidden by clearCalculator
    switch (calcType) {
        case 'Series':
            configVisual.src = 'series.gif';
            configVisual.style.display = 'block';
            break;
        case 'Parallel':
            configVisual.src = 'parallel.gif';
            configVisual.style.display = 'block';
            break;
        case 'NM':
            configVisual.src = 'nm.gif';
            configVisual.style.display = 'block';
            break;
        case 'NMWithSwitch':
            configVisual.src = 'nm_ws.gif';
            configVisual.style.display = 'block';
            break;
        default:
            configVisual.style.display = 'none'; // Hide if no valid selection
            break;
    }
}

// Ensure this function is called within the event listener for the dropdown change
document.getElementById('calcType').addEventListener('change', function() {
    handleCalcTypeChange(this.value);
});



function addAvailabilityInput() {
    var inputsContainer = document.getElementById('inputs');
    var newInput = document.createElement('input');
    newInput.type = 'text';
    newInput.placeholder = 'Enter availability (%)';
    inputsContainer.appendChild(newInput);
    newInput.focus(); // Focus on the new input field for user convenience
}

function clearCalculator(resetDropdown = true) {
    // Clear input values
    document.getElementById('inputs').innerHTML = '';
    document.querySelectorAll('#nmFields input').forEach(input => input.value = ''); // Clear N:M inputs
    document.getElementById('result').textContent = '---'; // Reset result display

    // Optionally reset the dropdown selection
    if (resetDropdown) {
        document.getElementById('calcType').selectedIndex = 0;
    }

    // Reset visibility to initial state without affecting the current calculation type selection
    document.getElementById('nmFields').style.display = 'none';
    document.getElementById('inputs').style.display = 'none';
    const switchFieldContainer = document.getElementById('switchAvailability').parentElement;
    switchFieldContainer.style.display = 'none'; // Ensure the switch field container is also hidden
}


function calculateAvailability() {
    var calcType = document.getElementById('calcType').value;
    var totalAvailability;

    switch (calcType) {
        case 'Series':
            totalAvailability = 1; // Start with 100% for series calculation
            document.querySelectorAll('#inputs input[type="text"]').forEach(input => {
                let availability = parseFloat(input.value) / 100;
                totalAvailability *= availability; // Multiply availabilities for series
            });
            break;

        case 'Parallel':
            totalAvailability = 1; // Start assuming complete unavailability for parallel calculation
            document.querySelectorAll('#inputs input[type="text"]').forEach(input => {
                let availability = parseFloat(input.value) / 100;
                totalAvailability *= (1 - availability); // Multiply unavailabilities
            });
            totalAvailability = 1 - totalAvailability; // Subtract from 1 to get total system availability
            break;

        case 'NM':
        case 'NMWithSwitch':
            let N = parseInt(document.getElementById('activeComponents').value) || 0;
            let M = parseInt(document.getElementById('standbyComponents').value) || 0;
            let A = parseFloat(document.getElementById('componentAvailability').value) / 100;
            totalAvailability = calculateNM(N, M, A);
            if (calcType === 'NMWithSwitch') {
                let switchAvailability = parseFloat(document.getElementById('switchAvailability').value) / 100;
                totalAvailability *= Math.pow(switchAvailability, 2); // Factor in switch availability as A^2
            }
            break;

        default:
            totalAvailability = 0; // Fallback for undefined calculation types
            break;
    }

    // Apply cap to the calculated availability
    if (totalAvailability > 0.99999999) {
        totalAvailability = 0.99999999; // Cap at 99.999999%
    }

    document.getElementById('result').textContent = (totalAvailability * 100).toFixed(6) + '%';
}


function calculateNM(N, M, A) {
    let total = 0;
    for (let i = 0; i <= M; i++) {
        total += binomialCoefficient(N + M, i) * Math.pow(1 - A, i) * Math.pow(A, N + M - i);
    }
    return total;
}

function binomialCoefficient(n, k) {
    let result = 1;
    if (k > n - k) {
        k = n - k;
    }
    for (let i = 0; i < k; ++i) {
        result *= (n - i);
        result /= (i + 1);
    }
    return result;
}


function clearInputs() {
    document.getElementById('inputs').innerHTML = ''; // Clear existing input fields
}


