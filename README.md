# acalc


## Description
This is an Availability Calculator, the calculator will allow for the user to enter the type of system: Series, Parallel, N:M or N:M with input and output switches. The calculator will then provide the calculated availability. The calculator is mainly written in JavaScript.<br>
When the docker container is started, it creates a 365 days self-signed certificate, the only port open is 443. There is a warning about un-trusted certificate when opening the page.<br>
The main scope of the calculator is to offer a generic availability calculator for the design engineer to use when preparing a design of a service or application.

## Getting started
You can modify the existing files before running the docker build command to make any branding adjustments. You can also have the option to pull the image from the dockerhub registry.

Build your own:

First you need to install docker by executing the install_docker.sh shell script depending on your OS. This install id for any Linux distribution<br>
`chmod +x install_docker.sh`<br>
`./install_docker.sh`

Run this command from same directory where the files are.<br>
`docker build -t [imagename] .`

You can run the container.<br>
`docker run -d --name [appname] --restart unless-stopped -p443:443 [imagename]`

If you prefer to run an existing image, then:<br>
`docker run -d --name acalc --restart unless-stopped -p443:443 rubenc2/acalc:latest`

## Working Demo
[This is a working demo app](https://52.206.89.44)

## Authors and acknowledgment
Ruben Calzadilla<br>

## License
MIT License apply.

## Project status
Always improving and willing to add any features you may have.
